;;;; Copyright (C) 2021 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;;;

(define-module (gitile config)
  #:use-module (srfi srfi-9)
  #:export (make-config
            config?
            <config>
            config-port
            config-host
            config-database
            config-repositories
            config-base-git-url
            config-index-title
            config-intro
            config-footer))

(define-record-type <config>
  (make-config port host database repositories base-git-url index-title intro footer)
  config?
  (port         config-port)
  (host         config-host)
  (database     config-database)
  (repositories config-repositories)
  (base-git-url config-base-git-url)
  (index-title  config-index-title)
  (intro        config-intro)
  (footer       config-footer))
