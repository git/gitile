;;;; Copyright (C) 2021 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;;;

(define-module (gitile highlight shell)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (syntax-highlight lexers)
  #:export (%shell-builtins
            %shell-keywords
            make-shell-lexer
            lex-shell
            make-m4-lexer
            lex-m4
            lex-autoconf))

(define %variable-char-set
  (char-set-union (char-set #\_) char-set:letter+digit))

(define %shell-builtins
  '("alias" "bg" "bind" "break" "builtin" "caller" "cd" "command" "compgen"
    "complete" "declare" "dirs" "disown" "echo" "enable" "eval" "exec" "exit"
    "export" "false" "fc" "fg" "getopts" "hash" "help" "history" "jobs" "kill"
    "let" "local" "logout" "popd" "printf" "pushd" "pwd" "read" "readonly" "set"
    "shift" "shopt" "source" "suspend" "test" "time" "times" "trap" "true"
    "type" "typeset" "ulimit" "umask" "unalias" "unset" "wait"))
(define %shell-keywords
  '("if" "fi" "else" "while" "in" "do" "done" "for" "then" "return" "function"
    "case" "select" "continue" "until" "esac" "elif"))

(define (make-shell-lexer builtins keywords)
  (define lex-shell-math
    (lex-any
      (lex-tag 'operator
               (apply lex-any (map lex-string
                                   '("-" "+" "*" "/" "%" "^" "|" "&" "||" "**"))))
      (lex-tag 'number (lex-all
                         (lex-char-set char-set:digit)
                         (lex-string "#")
                         (lex-char-set char-set:digit)))
      (lex-tag 'number (lex-char-set char-set:digit))
      shell-root-lexer))
  (define (shell-math-lexer tokens cursor)
    (lex-shell-math tokens cursor))

  (define lex-shell-curly
    (lex-any
      (lex-tag 'keyword (lex-string ":-"))
      (lex-tag 'variable (lex-char-set %variable-char-set))
      (lex-char-set (char-set-complement (char-set #\} #\: #\" #\' #\` #\" #\\)))
      (lex-string ":")
      shell-root-lexer))
  (define (shell-curly-lexer tokens cursor)
    (lex-shell-curly tokens cursor))
  
  (define lex-shell-interp
    (lex-any
      (lex-all (lex-tag 'keyword (lex-string "$(("))
               (lex-consume-until
                 (lex-tag 'keyword (lex-string "))"))
                 shell-math-lexer))
      (lex-all (lex-tag 'keyword (lex-string "$("))
               (lex-consume-until
                 (lex-tag 'keyword (lex-string ")"))
                 shell-root-lexer))
      (lex-all (lex-tag 'keyword (lex-string "${"))
               (lex-maybe (lex-tag 'keyword (lex-string "#")))
               (lex-consume-until
                 (lex-tag 'keyword (lex-string "}"))
                 shell-curly-lexer))
      (lex-tag 'variable (lex-all (lex-string "$") (lex-char-set %variable-char-set)))
      (lex-tag 'builtin
               (apply lex-any
                      (map lex-string '("$#" "$$" "$?" "$!" "$_" "$*" "$@"
                                        "$-" "$0" "$1" "$2" "$3" "$4" "$5"
                                        "$6" "$7" "$8" "$9"))))
      (lex-string "$")))
  (define (shell-interp-lexer tokens cursor)
    (lex-shell-interp tokens cursor))
  
  (define lex-shell-string
    (lex-any
      (apply lex-any (map lex-string '("\\" "\\." "\\$")))
      (lex-all (lex-string "\\") (lex-char-set (char-set #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7)))
      (lex-char-set (char-set-complement (char-set #\" #\\ #\$)))
      shell-interp-lexer))
  (define (shell-string-lexer tokens cursor)
    (lex-shell-string tokens cursor))

  (define (shell-heredoc-lexer tokens cursor)
    (define (get-delimiter str start)
      (let ((len (string-length str)))
        (let loop ((index start))
          (cond
            ((>= index len) (substring str start len))
            ((char-set-contains? (char-set-union %variable-char-set (char-set #\$))
                                 (string-ref str index))
             (loop (1+ index)))
            (else (substring str start index))))))

    (let*-values (((result remainder)
                   ((lex-all (lex-tag 'operator (lex-string "<<"))
                             (lex-maybe (lex-tag 'operator (lex-string "-")))
                             (lex-maybe lex-whitespace)
                             (lex-maybe (lex-string "\\")))
                    tokens cursor)))
      (if result
          (let* ((delimiter (get-delimiter (cursor-text remainder)
                                           (cursor-position remainder)))
                 (result (token-add tokens delimiter))
                 (remainder (move-cursor-by cursor (string-length delimiter))))
            (if (equal? delimiter "")
                (fail)
                ((lex-consume-until
                   (lex-string delimiter)
                   shell-string-lexer
                   #:tag 'string) result remainder)))
          (values result remainder))))

  (define lex-root
    (lex-any
      (lex-char-set char-set:whitespace)
      (lex-tag 'builtin (lex-filter
                          (lambda (str)
                            (any (cut string=? <> str) builtins))
                          (lex-char-set %variable-char-set)))
      (lex-tag 'keyword (lex-filter
                          (lambda (str)
                            (any (cut string=? <> str) keywords))
                          (lex-char-set %variable-char-set)))
      (lex-tag 'comment (lex-delimited "#" #:until "\n"))
      (lex-all (lex-tag 'variable (lex-char-set %variable-char-set))
               (lex-char-set char-set:whitespace)
               (lex-maybe (lex-string "+"))
               (lex-string "="))
      (lex-tag 'keyword (lex-string "`"))
      (lex-tag 'string (lex-delimited "'" #:escape #f))
      (lex-tag 'string (lex-delimited "$'" #:until "'" #:escape #f))
      (lex-all (lex-tag 'string (lex-any (lex-string "\"") (lex-string "$\"")))
               (lex-consume-until
                 (lex-string "\"")
                 shell-string-lexer
                 #:tag 'string))
      (lex-tag 'operator
               (apply lex-any (map lex-string '("[" "]" "{" "}" "(" ")" "="))))
      (lex-tag 'operator (lex-string "<<<"))
      shell-heredoc-lexer
      (lex-tag 'operator (lex-string "&&"))
      (lex-tag 'operator (lex-string "||"))
      (lex-string ";")
      (lex-string "&")
      (lex-string "|")
      (lex-char-set char-set:digit)
      (lex-string "\\\n")
      (lex-char-set
        (char-set-complement
          (char-set #\= #\[ #\] #\{ #\} #\( #\) #\$ #\" #\' #\` #\\ #\< #\& #\| #\; #\newline)))
      (lex-string "<")
      shell-interp-lexer))
  (define (shell-root-lexer tokens cursor)
    (lex-root tokens cursor))

  shell-root-lexer)

(define (lex-shell tokens cursor)
  ((lex-consume
    (make-shell-lexer %shell-builtins %shell-keywords))
   tokens cursor))

(define %macro-char-set (char-set-union char-set:letter+digit (char-set #\_)))

(define %m4-builtin-macros
  '("define" "undefine" "defn" "pushdef" "popdef" "ifdef" "shift"
    "changequote" "changecom" "divert" "undivert" "ifelse"
    "incr" "decr" "eval" "len" "dlen" "index" "index" "substr" "translit"
    "include" "sinclude" "syscmd" "maketemp" "m4exit" "m4wrap" "errprint"
    "dumpdef" "traceon" "traceoff" "divnum" "sysval"))

;; @defmacro in doc/autoconf.texi (autoconf sources)
(define %autoconf-builtin-macros
  '("AC_INIT" "AC_PACKAGE_NAME" "PACKAGE_NAME" "AC_PACKAGE_TARNAME"
    "PACKAGE_TARNAME" "AC_PACKAGE_VERSION" "PACKAGE_VERSION"
    "AC_PACKAGE_STRING" "PACKAGE_STRING" "AC_PACKAGE_BUGREPORT"
    "PACKAGE_BUGREPORT" "AC_PACKAGE_URL" "PACKAGE_URL"
    "AC_PREREQ" "AC_AUTOCONF_VERSION" "AC_COPYRIGHT" "AC_REVISION" "AC_CONFIG_SRCDIR"
    "AC_CONFIG_MACRO_DIRS" "AC_CONFIG_MACRO_DIR" "AC_CONFIG_AUX_DIR" "AC_REQUIRE_AUX_FILE"
    "AC_OUTPUT" "AC_PROG_MAKE_SET" "AC_CONFIG_FILES" "AC_CONFIG_HEADERS" "AH_HEADER"
    "AH_TEMPLATE" "AH_VERBATIM" "AH_TOP" "AH_BOTTOM" "AC_CONFIG_COMMANDS"
    "AC_CONFIG_COMMANDS_PRE" "AC_CONFIG_COMMANDS_POST" "AC_CONFIG_LINKS"
    "AC_CONFIG_SUBDIRS" "AC_PREFIX_DEFAULT" "AC_PREFIX_PROGRAM" "AC_INCLUDES_DEFAULT"
    "AC_CHECK_INCLUDES_DEFAULT" "AC_PROG_AWK" "AC_PROG_GREP" "AC_PROG_EGREP"
    "AC_PROG_FGREP" "AC_PROG_INSTALL" "AC_PROG_MKDIR_P" "AC_PROG_LEX" "AC_PROG_LN_S"
    "AC_PROG_RANLIB" "AC_PROG_SED" "AC_PROG_YACC" "AC_CHECK_PROG" "AC_CHECK_PROGS"
    "AC_CHECK_TARGET_TOOL" "AC_CHECK_TOOL" "AC_CHECK_TARGET_TOOLS" "AC_CHECK_TOOLS"
    "AC_PATH_PROG" "AC_PATH_PROGS" "AC_PATH_PROGS_FEATURE_CHECK" "AC_PATH_TARGET_TOOL"
    "AC_PATH_TOOL" "AC_CHECK_FILE" "AC_CHECK_FILES" "AC_CHECK_LIB" "AC_SEARCH_LIBS"
    "AC_FUNC_ALLOCA" "AC_FUNC_CHOWN" "AC_FUNC_CLOSEDIR_VOID" "AC_FUNC_ERROR_AT_LINE"
    "AC_FUNC_FNMATCH" "AC_FUNC_FNMATCH_GNU" "AC_FUNC_FORK" "AC_FUNC_FSEEKO"
    "AC_FUNC_GETGROUPS" "AC_FUNC_GETLOADAVG" "AC_FUNC_GETMNTENT" "AC_FUNC_GETPGRP"
    "AC_FUNC_LSTAT_FOLLOWS_SLASHED_SYMLINK" "AC_FUNC_MALLOC" "AC_FUNC_MBRTOWC"
    "AC_FUNC_MEMCMP" "AC_FUNC_MKTIME" "AC_FUNC_MMAP" "AC_FUNC_OBSTACK"
    "AC_FUNC_REALLOC" "AC_FUNC_SELECT_ARGTYPES" "AC_FUNC_SETPGRP" "AC_FUNC_STAT"
    "AC_FUNC_LSTAT" "AC_FUNC_STRCOLL" "AC_FUNC_STRERROR_R" "AC_FUNC_STRFTIME"
    "AC_FUNC_STRTOD" "AC_FUNC_STRTOLD" "AC_FUNC_STRNLEN" "AC_FUNC_UTIME_NULL"
    "AC_FUNC_VPRINTF" "AC_REPLACE_FNMATCH" "AC_CHECK_FUNC" "AC_CHECK_FUNCS"
    "AC_CHECK_FUNCS_ONCE" "AC_LIBOBJ" "AC_LIBSOURCE" "AC_LIBSOURCES"
    "AC_CONFIG_LIBOBJ_DIR" "AC_REPLACE_FUNCS" "AC_CHECK_HEADER_STDBOOL"
    "AC_HEADER_ASSERT" "AC_HEADER_DIRENT" "AC_HEADER_MAJOR" "AC_HEADER_RESOLV"
    "AC_HEADER_STAT" "AC_HEADER_STDBOOL" "AC_HEADER_STDC" "AC_HEADER_SYS_WAIT"
    "AC_HEADER_TIOCGWINSZ" "AC_CHECK_HEADER" "AC_CHECK_HEADERS" "AC_CHECK_HEADERS_ONCE"
    "AC_CHECK_DECL" "AC_CHECK_DECLS" "AC_CHECK_DECLS_ONCE" "AC_STRUCT_DIRENT_D_INO"
    "AC_STRUCT_DIRENT_D_TYPE" "AC_STRUCT_ST_BLOCKS" "AC_STRUCT_TM" "AC_STRUCT_TIMEZONE"
    "AC_CHECK_MEMBER" "AC_CHECK_MEMBERS" "AC_TYPE_GETGROUPS" "AC_TYPE_INT8_T"
    "AC_TYPE_INT16_T" "AC_TYPE_INT32_T" "AC_TYPE_INT64_T" "AC_TYPE_INTMAX_T"
    "AC_TYPE_INTPTR_T" "AC_TYPE_LONG_DOUBLE" "AC_TYPE_LONG_DOUBLE_WIDER"
    "AC_TYPE_LONG_LONG_INT" "AC_TYPE_MBSTATE_T" "AC_TYPE_MODE_T" "AC_TYPE_OFF_T"
    "AC_TYPE_PID_T" "AC_TYPE_SIZE_T" "AC_TYPE_SSIZE_T" "AC_TYPE_UID_T"
    "AC_TYPE_UINT8_T" "AC_TYPE_UINT16_T" "AC_TYPE_UINT32_T" "AC_TYPE_UINT64_T"
    "AC_TYPE_UINTMAX_T" "AC_TYPE_UINTPTR_T" "AC_TYPE_UNSIGNED_LONG_LONG_INT"
    "AC_CHECK_TYPE" "AC_CHECK_TYPES" "AC_CHECK_SIZEOF" "AC_CHECK_ALIGNOF"
    "AC_COMPUTE_INT" "AC_LANG_WERROR" "AC_OPENMP" "AC_PROG_CC" "AC_PROG_CC_C_O"
    "AC_PROG_CPP" "AC_PROG_CPP_WERROR" "AC_C_BACKSLASH_A" "AC_C_BIGENDIAN"
    "AC_C_CONST" "AC_C__GENERIC" "AC_C_RESTRICT" "AC_C_VOLATILE" "AC_C_INLINE"
    "AC_C_CHAR_UNSIGNED" "AC_C_STRINGIZE" "AC_C_FLEXIBLE_ARRAY_MEMBER"
    "AC_C_VARARRAYS" "AC_C_TYPEOF" "AC_C_PROTOTYPES" "AC_PROG_GCC_TRADITIONAL"
    "AC_PROG_CXX" "AC_PROG_CXXCPP" "AC_PROG_CXX_C_O" "AC_PROG_OBJC" "AC_PROG_OBJCPP"
    "AC_PROG_OBJCXX" "AC_PROG_OBJCXXCPP" "AC_ERLANG_PATH_ERLC" "AC_ERLANG_NEED_ERLC"
    "AC_ERLANG_PATH_ERL" "AC_ERLANG_NEED_ERL" "AC_PROG_F77" "AC_PROG_FC"
    "AC_PROG_F77_C_O" "AC_PROG_FC_C_O" "AC_F77_LIBRARY_LDFLAGS" "AC_FC_LIBRARY_LDFLAGS"
    "AC_F77_DUMMY_MAIN" "AC_FC_DUMMY_MAIN" "AC_F77_MAIN" "AC_FC_MAIN" "AC_F77_WRAPPERS"
    "AC_FC_WRAPPERS" "AC_F77_FUNC" "AC_FC_FUNC" "AC_FC_SRCEXT" "AC_FC_PP_SRCEXT"
    "AC_FC_PP_DEFINE" "AC_FC_FREEFORM" "AC_FC_FIXEDFORM" "AC_FC_LINE_LENGTH"
    "AC_FC_CHECK_BOUNDS" "AC_F77_IMPLICIT_NONE" "AC_FC_IMPLICIT_NONE"
    "AC_FC_MODULE_EXTENSION" "AC_FC_MODULE_FLAG" "AC_FC_MODULE_OUTPUT_FLAG"
    "AC_PROG_GO" "AC_PATH_X" "AC_PATH_XTRA" "AC_SYS_INTERPRETER" "AC_SYS_LARGEFILE"
    "AC_SYS_LONG_FILE_NAMES" "AC_SYS_POSIX_TERMIOS" "AC_USE_SYSTEM_EXTENSIONS"
    "AC_ERLANG_SUBST_ERTS_VER" "AC_ERLANG_SUBST_ROOT_DIR" "AC_ERLANG_SUBST_LIB_DIR"
    "AC_ERLANG_CHECK_LIB" "AC_ERLANG_SUBST_INSTALL_LIB_DIR"
    "AC_ERLANG_SUBST_INSTALL_LIB_SUBDIR" "AC_LANG" "AC_LANG_PUSH" "AC_LANG_POP"
    "AC_LANG_ASSERT" "AC_REQUIRE_CPP" "AC_LANG_CONFTEST" "AC_LANG_DEFINES_PROVIDED"
    "AC_LANG_SOURCE" "AC_LANG_PROGRAM" "AC_LANG_CALL" "AC_LANG_FUNC_LINK_TRY"
    "AC_PREPROC_IFELSE" "AC_EGREP_HEADER" "AC_EGREP_CPP" "AC_COMPILE_IFELSE"
    "AC_LINK_IFELSE" "AC_RUN_IFELSE" "AC_DEFINE" "AC_DEFINE" "AC_DEFINE_UNQUOTED"
    "AC_DEFINE_UNQUOTED" "AC_SUBST" "AC_SUBST_FILE" "AC_ARG_VAR" "AC_CACHE_VAL"
    "AC_CACHE_CHECK" "AC_CACHE_LOAD" "AC_CACHE_SAVE" "AC_MSG_CHECKING"
    "AC_MSG_RESULT" "AC_MSG_NOTICE" "AC_MSG_ERROR" "AC_MSG_FAILURE" "AC_MSG_WARN"
    "__file__" "__line__" "__oline__" "dnl" "m4_bpatsubst" "m4_bregexp"
    "m4_copy" "m4_copy_force" "m4_rename" "m4_rename_force" "m4_defn"
    "m4_divert" "m4_dumpdef" "m4_dumpdefs" "m4_esyscmd_s" "m4_exit" "m4_if"
    "m4_include" "m4_sinclude" "m4_mkstemp" "m4_maketemp" "m4_popdef"
    "m4_undefine" "m4_undivert" "m4_wrap" "m4_wrap_lifo" "m4_assert" "m4_errprintn"
    "m4_fatal" "m4_location" "m4_warn" "m4_cleardivert" "m4_divert_once"
    "m4_divert_pop" "m4_divert_push" "m4_divert_text" "m4_init" "m4_bmatch"
    "m4_bpatsubsts" "m4_case" "m4_cond" "m4_default" "m4_default_quoted"
    "m4_default_nblank" "m4_default_nblank_quoted" "m4_define_default"
    "m4_ifblank" "m4_ifnblank" "m4_ifndef" "m4_ifset" "m4_ifval" "m4_ifvaln"
    "m4_n" "m4_argn" "m4_car" "m4_cdr" "m4_for" "m4_foreach" "m4_foreach_w"
    "m4_map" "m4_mapall" "m4_map_sep" "m4_mapall_sep" "m4_map_args"
    "m4_map_args_pair" "m4_map_args_sep" "m4_map_args_w" "m4_shiftn" "m4_shift2"
    "m4_shift3" "m4_stack_foreach" "m4_stack_foreach_lifo" "m4_stack_foreach_sep"
    "m4_stack_foreach_sep_lifo" "m4_apply" "m4_count" "m4_curry" "m4_do"
    "m4_dquote" "m4_dquote_elt" "m4_echo" "m4_expand" "m4_ignore" "m4_make_list"
    "m4_quote" "m4_reverse" "m4_unquote" "m4_append" "m4_append_uniq"
    "m4_append_uniq_w" "m4_chomp" "m4_chomp_all" "m4_combine" "m4_escape"
    "m4_flatten" "m4_join" "m4_joinall" "m4_newline" "m4_normalize" "m4_re_escape"
    "m4_split" "m4_strip" "m4_text_box" "m4_text_wrap" "m4_tolower" "m4_toupper"
    "m4_cmp" "m4_list_cmp" "m4_max" "m4_min" "m4_sign" "m4_version_compare"
    "m4_version_prereq" "m4_set_add" "m4_set_add_all" "m4_set_contains"
    "m4_set_contents" "m4_set_dump" "m4_set_delete" "m4_set_difference"
    "m4_set_intersection" "m4_set_union" "m4_set_empty" "m4_set_foreach"
    "m4_set_list" "m4_set_listc" "m4_set_map" "m4_set_map_sep" "m4_set_remove"
    "m4_set_size" "m4_pattern_forbid" "m4_pattern_allow" "AS_BASENAME"
    "AS_BOX" "AS_CASE" "AS_DIRNAME" "AS_ECHO" "AS_ECHO_N" "AS_ESCAPE"
    "AS_EXECUTABLE_P" "AS_EXIT" "AS_IF" "AS_MKDIR_P" "AS_SET_STATUS" "AS_TR_CPP"
    "AS_TR_SH" "AS_SET_CATFILE" "AS_UNSET" "AS_VERSION_COMPARE" "AS_LITERAL_IF"
    "AS_LITERAL_WORD_IF" "AS_VAR_APPEND" "AS_VAR_ARITH" "AS_VAR_COPY" "AS_VAR_IF"
    "AS_VAR_PUSHDEF" "AS_VAR_POPDEF" "AS_VAR_SET" "AS_VAR_SET_IF" "AS_VAR_TEST_SET"
    "AS_BOURNE_COMPATIBLE" "AS_INIT" "AS_INIT_GENERATED" "AS_LINENO_PREPARE"
    "AS_ME_PREPARE" "AS_TMPDIR" "AS_SHELL_SANITIZE" "AS_MESSAGE_FD" "AS_MESSAGE_LOG_FD"
    "AS_ORIGINAL_STDIN_FD" "AC_DEFUN" "AC_REQUIRE" "AC_BEFORE" "AC_DEFUN_ONCE"
    "AU_DEFUN" "AU_ALIAS" "AC_CANONICAL_BUILD" "AC_CANONICAL_HOST" "AC_CANONICAL_TARGET"
    "AC_PRESERVE_HELP_ORDER" "AC_ARG_WITH" "AC_ARG_ENABLE" "AS_HELP_STRING"
    "AC_DISABLE_OPTION_CHECKING" "AC_ARG_PROGRAM" "AC_AIX" "AC_ALLOCA" "AC_ARG_ARRAY"
    "AC_C_CROSS" "AC_C_LONG_DOUBLE" "AC_CANONICAL_SYSTEM" "AC_CHAR_UNSIGNED"
    "AC_CHECK_TYPE" "AC_CHECKING" "AC_COMPILE_CHECK" "AC_CONST" "AC_CROSS_CHECK"
    "AC_CYGWIN" "AC_DECL_SYS_SIGLIST" "AC_DECL_YYTEXT" "AC_DIAGNOSE" "AC_DIR_HEADER"
    "AC_DYNIX_SEQ" "AC_EXEEXT" "AC_EMXOS2" "AC_ENABLE" "AC_ERROR" "AC_FATAL"
    "AC_FIND_X" "AC_FIND_XTRA" "AC_FOREACH" "AC_FUNC_CHECK" "AC_FUNC_SETVBUF_REVERSED"
    "AC_FUNC_WAIT3" "AC_GCC_TRADITIONAL" "AC_GETGROUPS_T" "AC_GETLOADAVG" "AC_GNU_SOURCE"
    "AC_HAVE_FUNCS" "AC_HAVE_HEADERS" "AC_HAVE_LIBRARY" "AC_HAVE_POUNDBANG"
    "AC_HEADER_CHECK" "AC_HEADER_EGREP" "AC_HEADER_TIME" "AC_HELP_STRING"
    "AC_INLINE" "AC_INT_16_BITS" "AC_IRIX_SUN" "AC_ISC_POSIX" "AC_LANG_C"
    "AC_LANG_CPLUSPLUS" "AC_LANG_FORTRAN77" "AC_LANG_RESTORE" "AC_LANG_SAVE"
    "AC_LINK_FILES" "AC_LN_S" "AC_LONG_64_BITS" "AC_LONG_DOUBLE" "AC_LONG_FILE_NAMES"
    "AC_MAJOR_HEADER" "AC_MEMORY_H" "AC_MINGW32" "AC_MINIX" "AC_MINUS_C_MINUS_O"
    "AC_MMAP" "AC_MODE_T" "AC_OBJEXT" "AC_OBSOLETE" "AC_OFF_T" "AC_OUTPUT"
    "AC_OUTPUT_COMMANDS" "AC_PID_T" "AC_PREFIX" "AC_PROG_CC_C89" "AC_PROG_CC_C99"
    "AC_PROG_CC_STDC" "AC_PROGRAMS_CHECK" "AC_PROGRAMS_PATH" "AC_PROGRAM_CHECK"
    "AC_PROGRAM_EGREP" "AC_PROGRAM_PATH" "AC_REMOTE_TAPE" "AC_RESTARTABLE_SYSCALLS"
    "AC_RETSIGTYPE" "AC_RSH" "AC_SCO_INTL" "AC_SETVBUF_REVERSED" "AC_SET_MAKE"
    "AC_SIZEOF_TYPE" "AC_SIZE_T" "AC_STAT_MACROS_BROKEN" "AC_STDC_HEADERS"
    "AC_STRCOLL" "AC_STRUCT_ST_BLKSIZE" "AC_STRUCT_ST_RDEV" "AC_ST_BLKSIZE"
    "AC_ST_BLOCKS" "AC_ST_RDEV" "AC_SYS_RESTARTABLE_SYSCALLS" "AC_SYS_SIGLIST_DECLARED"
    "AC_TEST_CPP" "AC_TEST_PROGRAM" "AC_TIMEZONE" "AC_TIME_WITH_SYS_TIME"
    "AC_TRY_COMPILE" "AC_TRY_CPP" "AC_TRY_LINK" "AC_TRY_LINK_FUNC"
    "AC_TRY_RUN" "AC_TYPE_SIGNAL" "AC_UID_T" "AC_UNISTD_H" "AC_USG" "AC_UTIME_NULL"
    "AC_VALIDATE_CACHED_SYSTEM_TUPLE" "AC_VERBOSE" "AC_VFORK" "AC_VPRINTF"
    "AC_WAIT3" "AC_WARN" "AC_WARNING" "AC_WITH" "AC_WORDS_BIGENDIAN" "AC_XENIX_DIR"
    "AC_YYTEXT_POINTER" "AT_INIT" "AT_COPYRIGHT" "AT_ARG_OPTION" "AT_ARG_OPTION_ARG"
    "AT_COLOR_TESTS" "AT_TESTED" "AT_PREPARE_TESTS" "AT_PREPARE_EACH_TEST"
    "AT_TEST_HELPER_FN" "AT_BANNER" "AT_SETUP" "AT_KEYWORDS" "AT_CAPTURE_FILE"
    "AT_FAIL_IF" "AT_SKIP_IF" "AT_XFAIL_IF" "AT_CLEANUP" "AT_DATA" "AT_DATA_UNQUOTED"
    "AT_CHECK" "AT_CHECK_UNQUOTED" "AT_CHECK_EUNIT" "AC_CONFIG_TESTDIR"))

(define (make-m4-lexer base-lexer builtins quote-in quote-out)
  (define lex-macro-argument
    (lex-any
      (lex-tag 'comment (lex-delimited "#" #:until "\n"))
      (lex-tag 'comment (lex-delimited "dnl" #:until "\n"))
      (lex-tag 'comment (lex-delimited "DNL" #:until "\n"))
      (lex-string ",")
      lex-whitespace
      (lex-tag 'string (lex-delimited quote-in #:until quote-out #:nested? #t))
      (lex-char-set (char-set-complement (char-set #\, #\))))))

  (define lex-macro
    (lex-all
      (lex-any
        (lex-tag 'builtin (lex-filter
                            (lambda (str)
                              (any (cut string=? <> str) builtins))
                            (lex-char-set %macro-char-set)))
        (lex-tag 'variable (lex-char-set %macro-char-set)))
      (lex-maybe (lex-all
                   (lex-tag 'open (lex-string "("))
                   (lex-consume-until
                     (lex-tag 'close (lex-string ")"))
                     lex-macro-argument)))))

  (lex-consume
    (lex-any
      (lex-tag 'comment (lex-delimited "#" #:until "\n"))
      (lex-tag 'comment (lex-delimited "dnl" #:until "\n"))
      (lex-tag 'comment (lex-delimited "DNL" #:until "\n"))
      lex-whitespace
      lex-macro
      base-lexer)))

(define (lex-m4 tokens cursor)
  ((make-m4-lexer lex-fail %m4-builtin-macros "`" "'")
   tokens cursor))

(define (lex-autoconf tokens cursor)
  ((make-m4-lexer (make-shell-lexer %shell-builtins %shell-keywords)
                 (append %m4-builtin-macros %autoconf-builtin-macros)
                 "[" "]")
   tokens cursor))
