;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;;;

(define-module (gitile pages)
  #:use-module (commonmark)
  #:use-module (gcrypt base16)
  #:use-module (gcrypt hash)
  #:use-module (gitile code)
  #:use-module (gitile repo)
  #:use-module (git)
  #:use-module (git types)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1); fold
  #:use-module (srfi srfi-19); date/time
  #:use-module (system foreign)
  #:use-module (web uri)
  #:export (not-yet-page
            project-file-raw
            project-files
            project-index
            project-commits
            project-commit
            project-tags))

(define not-yet-page
  `((p "Not yet available, sorry :/")))

(define* (project-file-raw repo path #:key (ref "-"))
  (let* ((ref (if (equal? ref "-") #f ref))
         (path (canonalize-path path)))
    (get-file-content repo path #:ref ref)))

(define (canonalize-path path)
  (if (list? path)
     (string-join
       (filter (lambda (p)
                 (and (not (string-null? p))
                      (not (equal? p "."))))
               path)
       "/")
     path))

(define (file-content repo path ref repository-name)
  (define (ensure-relative-link link)
    "If link is relative, ensure it resolves relative to repo's base dir, in
current ref."
    (if (string->uri link)
        link
        (string-append "/" repository-name "/tree/" (or ref "-") "/" link)))

  (define improve-content
    (match-lambda
      ((? string? s) s)
      (('a ('@ arg ...) content ...)
       `(a (@ (rel "nofollow")
              ,@(map
                  (match-lambda
                    (('href link)
                     `(href ,(ensure-relative-link link)))
                    (('rel _)
                     `(rel "nofollow"))
                    (arg arg))
                  arg))
           ,@content))
      (('code ('@ ('class language)) content)
       `(code (@ (class ,language))
              ,(display-formatted-code
                 content
                 (if (string-prefix? "language-" language)
                     (substring language 9)
                     "unknown"))))
      ((tag ('@ arg ...) content ...)
       `(,tag (@ ,@arg) ,@(map improve-content content)))
      ((tag content ...)
       `(,tag ,@(map improve-content content)))))

  `((div (@ (class "content"))
     (p ,(basename path))
     (p (@ (class "button-row"))
       (a (@ (href "/" ,repository-name "/raw/" ,(or ref "-")
                   "/" ,path))
          (img (@ (src "/images/file-type-3.svg"))))))
    ,(let ((content (utf8->string (get-file-content repo path #:ref ref))))
       (if (string-suffix? ".md" path)
         `(article (@ (class "formatted-file-content"))
            ,(improve-content (commonmark->sxml content)))
         (display-code content path)))))

(define* (project-files repository-name repo #:key (ref "-") (path '()))
  (let* ((ref (if (equal? ref "-") #f ref))
         (path-box (parent-paths-box repository-name ref path))
         (path (canonalize-path path))
         (dir-path (if (string-null? path) path (string-append path "/")))
         (files (sort (get-files repo #:ref ref #:path dir-path)
                      (lambda (f1 f2)
                        (string<=? (file-name f1) (file-name f2)))))
         (readme (filter
                   (lambda (f)
                     (or (string-ci=? (file-name f) "README.md")
                         (string-ci=? (file-name f) "README")))
                   files)))
    (if (and (string-null? path) (null? files))
        `(p "Empty repository")
        (if (null? files)
            `(,path-box
              ,(file-content repo path ref repository-name))
            `(,(last-commit-infobox repository-name repo ref)
              ,path-box
              (table
                (thead
                  (tr
                    (td "name")
                    (td "last commit")
                    (td "date")))
                (tbody
                 ,@(map
                     (match-lambda
                       (($ <file> name type commit)
                        (let ((name (if (= type 2) (string-append name "/") name)))
                          `(tr (td (a (@ (href "/" ,repository-name
                                               "/tree/" ,(or ref "-")
                                               "/" ,path "/" ,name)
                                         (class "icon-link"))
                                      (img (@ (src "/images/file-type-" ,type ".svg")))
                                      (span ,name)))
                               (td (a (@ (href "/" ,repository-name
                                               "/commit/" ,(oid->string
                                                             (commit-id commit))))
                                      ,(commit-summary commit)))
                               (td ,(time->date-string (commit-time commit)))))))
                     files)))
              ,@(if (null? readme)
                    '()
                    (file-content
                      repo
                      (string-append path (if (equal? path "") "" "/")
                                     (file-name (car readme)))
                      ref
                      repository-name)))))))

(define get-config-string
  (let ((proc (libgit2->procedure int "git_config_get_string" '(* * *))))
    (lambda (config name)
      (let ((out (make-double-pointer)))
        (proc out (config->pointer config) (string->pointer name))
        (pointer->string out)))))

(define (project-index base-url repository-name repo)
  `((h1 ,repository-name)
    (p ,(get-synopsis repo))
    (p (@ (class "clone"))
       (code "git clone " ,base-url "/" ,repository-name))
    ,(project-files repository-name repo)))

(define (author-image author)
  (string-append "https://avatar.lepiller.eu/cat-avatar-generator.php?seed="
                 (bytevector->base16-string
                   (bytevector-hash
                     (string->utf8 (signature-email author))
                     (hash-algorithm sha1)))))

(define (time->date-string time)
  (date->string
    (time-utc->date
      (make-time time-utc 0 time))))

(define (last-commit-infobox repository-name repo ref)
  (let* ((ref (if (equal? ref "-") #f ref))
         (commit (last-commit repo ref)))
    (commit-infobox repository-name commit)))

(define* (commit-infobox repository-name commit #:key (open? #f) (detailed? #f))
  `(div (@ (class "commit-info"))
     (p (img (@ (src ,(author-image (commit-author commit))))))
     (div (@ (class "commit"))
       (p (@ (class "message"))
          (a (@ (href "/" ,repository-name "/commit/"
                      ,(oid->string (commit-id commit))))
             ,(commit-summary commit)))
       (p (span (@ (class "author"))
                ,(signature-name (commit-author commit)))
          (span (@ (class "date"))
            ,(time->date-string (commit-time commit)))))
     (div (@ (class "commit-id"))
          (p (@ (class "short-id"))
             ,(string-take (oid->string (commit-id commit)) 7))
          (button (@ (data-clipboard-copy ,(oid->string (commit-id commit)))
                     (class "copy"))
              (img (@ (src "/images/copy.svg"))))
          ,(if open?
               `(a (@ (href "/" ,repository-name "/tree/"
                            ,(oid->string (commit-id commit))))
                    (img (@ (src "/images/go.svg"))))
           '()))))

(define (project-commits repository-name repo ref)
  (let* ((commits (get-commits repo ref))
         (next (cdr commits))
         (commits (car commits)))
    `(,(map (lambda (commit)
              (commit-infobox repository-name commit #:open? #t))
            commits)
      ,(if next
           `(p (a (@ (href "/" ,repository-name "/commits/"
                           ,(oid->string (commit-id next))))))
           '()))))

(define (parent-paths-box repository-name ref path)
  `(div (@ (class "path-box"))
    ,(if (null? (pk 'path path))
         `(a (@ (href "/" ,repository-name)) ,repository-name)
         (fold
           (lambda (p res)
             (cons* p "/" res))
           (list `(a ,(car (reverse path))))
           (map
             (lambda (p)
               (let ((p (if (string-null? p) p (substring p 1)))
                     (base (if (string-null? (basename p))
                               repository-name
                               (basename p))))
                 `(a (@ (href "/" ,repository-name "/tree/" ,(or ref "-") "/" ,p))
                     ,base)))
             (fold
               (lambda (p res)
                 (cons (string-append (car res) "/" p)
                       res))
               (list "")
               (reverse (cdr (reverse path)))))))))

(define (project-tags repository-name repo)
  (let ((tags (get-tags repo)))
    (pk 'tags tags)
    (if (null? tags)
        `((p "This project has no tags yet."))
        (map (lambda (tag) (tag-box repository-name repo tag)) tags))))

(define (tag-box repository-name repo tag)
  (match tag
    (($ <mytag> name message target time)
     (let ((commit (get-commit repo target)))
       `(div (@ (class "tag-box"))
             (p (img (@ (src ,(author-image (commit-author commit))))))
             (div (@ (class "tag"))
                  (p (@ (class "tag-name"))
                     (a (@ (href "/" ,repository-name "/tree/" ,target)) ,name))
                  (p ,message)
                  (p (span (@ (class "author"))
                           ,(signature-name (commit-author commit)))
                     (span (@ (class "date"))
                           ,(time->date-string time)))))))))

(define (project-commit repository-name repo ref)
  (let* ((commit (get-commit repo ref))
         (parent (commit-parent commit))
         (diff (diff-tree-to-tree repo (commit-tree parent) (commit-tree commit))))
    `(,(commit-infobox repository-name commit #:open? #t)
      (p (@ (class "commit-summary")) ,(commit-message commit))
      ,(diff-box diff))))

(define (diff-box diff)
  (let ((content '()) (file-header #f) (file-content #f) (first-hunk #t))
    (diff-foreach diff
      (lambda (delta progress)
        (when file-header
          (set! content (append content (cons file-header `((table (@ (class "file-diff")) ,file-content))))))
        (set! first-hunk #t)
        (set! file-content '())
        (cond
          ((equal? (diff-delta-status delta) GIT-DELTA-MODIFIED)
           (set! file-header
             `(p (@ (class "diff-file-name")) ,(diff-file-path (diff-delta-old-file delta)))))
          (else
            (set! file-header
              `(p (@ (class "diff-file-name"))
                  ,(diff-file-path (diff-delta-old-file delta))
                  " unknown status "
                  ,(number->string (diff-delta-status delta))))))
        0)
      (lambda (delta binary)
        (set! file-content
          `((tr (@ (class "diff-no-data")) (td (@ (colspan 4)) (p "Binary data")))))
        0)
      (lambda (delta hunk)
        (if first-hunk
            (set! first-hunk #f)
            (set! file-content
              (append file-content `((tr (@ (class "hunk-delim"))
                                         (td (@ (colspan 4)) (p "…")))))))
        0)
      (lambda (delta hunk line)
        (let* ((origin (list->string (list (integer->char (diff-line-origin line)))))
               (class (match origin
                        ("-" "diff-minus")
                        ("+" "diff-plus")
                        (_ ""))))
          (let loop ((lines (string-split (diff-line-content line) #\newline))
                     (old-line (diff-line-old-lineno line))
                     (new-line (diff-line-new-lineno line)))
            (match lines
              (() #t)
              (("") #t)
              ((line lines ...)
               (set! file-content
                 (append file-content `((tr (@ (class ,class))
                                          (td (@ (class "diff-line-num"))
                                              ,(if (= old-line -1) "" old-line))
                                          (td (@ (class "diff-line-num"))
                                              ,(if (= new-line -1) "" new-line))
                                          (td ,origin)
                                          (td (@ (class "diff-line"))
                                              (pre ,line))))))
               (loop lines (+ old-line 1) (+ new-line 1))))))
        0))
    (append content (cons file-header `((table (@ (class "file-diff")) ,file-content))))))
