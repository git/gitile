;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;;;

(define-module (gitile handler)
  #:use-module (fibers web server)
  #:use-module (git)
  #:use-module (gitile config)
  #:use-module (gitile pages)
  #:use-module (gitile repo)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (sxml simple)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web uri)
  #:export (gitile-handler))

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define (not-found request footer)
  (format #t "Not found: ~a~%" (uri->string (request-uri request)))
  (show (style `((p "Resource not found: "
                    ,(uri->string (request-uri request))))
               "" "" footer)
        #:code 404))

(define (style page project ref footer)
  `(html
     (head
       (meta (@ (charset "UTF-8")))
       (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
       (link (@ (rel "stylesheet") (href "/css/gitile.css")))
       (link (@ (rel "stylesheet") (href "/css/highlight.css")))
       (link (@ (rel "icon") (href "/images/icon.png") (sizes "32x32")))
       (script (@ (src "/js/gitile.js")) "")
       (title ,project))
     (body
       (header
         (nav
           (ul
             (li (@ (class "first")) (a (@ (href "/")) "Projects"))
             (li (a (@ (href "/" ,project)) "Repository"))
             (li (a (@ (href "/" ,project "/tree/" ,ref)) "Files"))
             (li (a (@ (href "/" ,project "/commits")) "Commits"))
             (li (a (@ (href "/" ,project "/tags")) "Tags")))))
       (div (@ (id "content"))
            ,@page)
       (footer ,@footer))))

(define-record-type <project>
  (make-project slug name description)
  project?
  (slug project-slug)
  (name project-name)
  (description project-description))

(define (index-page projects base-git-url title intro footer)
  (pk 'projects projects)
  `(html
     (head
       (meta (@ (charset "UTF-8")))
       (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
       (link (@ (rel "stylesheet") (href "/css/git.css")))
       (link (@ (rel "icon") (href "/images/icon.png") (sizes "32x32")))
       (title ,title))
     (body (@ (lang "en"))
       (header (@ (id "intro"))
         (h1 "Every project")
         ,@intro
         (p (@ (id "mire"))
            (span (@ (class "blue")) "")
            (span (@ (class "red")) "")
            (span (@ (class "green")) "")
            (span (@ (class "orange")) "")))
       (div (@ (id "content"))
         (div (@ (id "project-list"))
           ,@(map
               (lambda (project)
                 `(div (@ (class "project"))
                    (h1 (a (@ (href ,(string-append "/" (project-slug project))))
                           ,(project-name project)))
                    ,@(match (xml->sxml (string-append "<d>" (project-description project) "</d>"))
                        (('*TOP* ('d content ...))
                         content))
                    (div (@ (class "instructions"))
                         (code "git clone " ,base-git-url "/"
                               ,(project-slug project)))))
               projects)))
       (footer ,@footer))))

(define* (show page #:key (code 200))
  (values (build-response #:code code #:headers '((content-type . (text/html))))
          (with-output-to-string (lambda _ (sxml->xml page)))))

(define (show-raw page)
  (values '((content-type . (text/plain)))
          page))

(define (gitile-handler config)
  (define (repo-404 content project-name ref)
    (show (style content project-name ref (config-footer config))
          #:code 404))

  (define (repo-500 content)
    (show content #:code 500))

  (define (call-with-repo project-name callback)
    (define (get-repo name)
      (let ((repo (string-append (config-repositories config)
                                 "/" name ".git"))
            (repo2 (string-append (config-repositories config)
                                  "/" name)))
        (cond
          ((file-exists? (string-append repo "/git-daemon-export-ok"))
           (repository-open repo))
          ((file-exists? (string-append repo2 "/git-daemon-export-ok"))
           (repository-open repo2))
          (else #f))))
  
    (let ((repo (get-repo project-name)))
      (if repo
          (catch #t
            (lambda _
              (callback repo))
            (lambda* (key . args)
              (match key
                ('not-found
                 (pk 'except key args)
                 (repo-404 (assoc-ref args 'content)
                           project-name
                           (or (assoc-ref args 'ref) "-")))
                (_
                  (pk 'uncaught-exception key args)
                  (repo-500 `((p "Internal error")))))))
          (repo-404 `((p "Repository not found: " ,project-name)) project-name
                    "-"))))

  (define (name->project project)
    (make-project project (call-with-repo project get-name)
                  (call-with-repo project get-description)))

  (define (projects)
    (define (projects-aux dir)
      (let ((opened-dir (opendir dir)))
        (let loop ((res '()) (repo (readdir opened-dir)))
          (cond
            ((eof-object? repo)
             (closedir opened-dir)
             res)
            ((file-exists? (string-append dir "/" repo "/git-daemon-export-ok"))
             (loop
               (cons
                 (name->project
                   (substring
                     (string-append
                       dir "/"
                       (if (string-suffix? ".git" repo)
                           (substring repo 0 (- (string-length repo) 4))
                           repo))
                     (+ (string-length (config-repositories config)) 1)))
                 res)
               (readdir opened-dir)))
            ((file-exists? (string-append dir "/" repo "/.git"))
             (loop res (readdir opened-dir)))
            ((string-suffix? ".git" repo)
             (loop res (readdir opened-dir)))
            ((and (stat (string-append dir "/" repo) #f)
                  (equal? (stat:type (stat (string-append dir "/" repo) #f)) 'directory)
                  (not (equal? repo ".git"))
                  (not (equal? repo "."))
                  (not (equal? repo "..")))
             (loop (append (projects-aux (string-append dir "/" repo)) res)
                   (readdir opened-dir)))
            (else (loop res (readdir opened-dir)))))))
    (projects-aux (config-repositories config)))

  (define (find-project-component request)
    (let ((request (string-join
                     (split-and-decode-uri-path (uri-path (request-uri request)))
                     "/")))
      (let loop ((projects (projects)))
        (match projects
          (() (values "" (split-and-decode-uri-path request)))
          ((project projects ...)
           (let ((slug (project-slug project)))
           (if (or (string-prefix? (string-append slug "/") request)
                   (equal? slug request))
               (values (project-slug project)
                       (split-and-decode-uri-path
                         (substring request (string-length slug))))
               (loop projects))))))))

  (match config
    (($ <config> port host database repositories base-git-url index-title intro
        footer)
     (lambda (request body)
       (pk 'request request)
       (pk 'body (if body (utf8->string body) body))
       (let-values (((project-name path) (find-project-component request)))
         (match (cons project-name path)
           (("" . ())
            (show (index-page (projects) base-git-url index-title intro footer)))
           (("" . args)
            (repo-404 "Project not found" ""
                      "-"))
           ((project-name)
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-index base-git-url project-name repo)
                             project-name "-" footer)))))
           ((project-name "tree" ref path ...)
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-files project-name repo #:ref ref #:path path)
                             project-name ref footer)))))
           ((project-name "raw" ref path ...)
            (call-with-repo project-name
              (lambda (repo)
                (show-raw (project-file-raw repo path #:ref ref)))))
           ((project-name "commits")
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-commits project-name repo #f)
                      project-name "-" footer)))))
           ((project-name "commits" ref)
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-commits project-name repo ref)
                      project-name ref footer)))))
           ((project-name "commit" ref)
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-commit project-name repo ref)
                             project-name ref footer)))))
           ((project-name "tags")
            (call-with-repo project-name
              (lambda (repo)
                (show (style (project-tags project-name repo)
                      project-name "-" footer)))))
           (_ (not-found request footer))))))))
