// SPDX-FileCopyrightText: 2021 Julien Lepiller <julien@lepiller.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

window.onload = function() {
  buttons = document.getElementsByClassName('copy')

  function mycopy(button) {
    alert(button)
    data = button.dataset.clipboardCopy
    while(data == undefined) {
          button = button.parentElement
          data = button.dataset.clipboardCopy
    }
    alert(data)
    i = document.createElement('input')
    i.type = 'text'
    i.value = data
    i.display = 'none'
    button.appendChild(i)
    i.select()
    i.setSelectionRange(0, 99999); /* For mobile devices */
    document.execCommand("copy")
    button.removeChild(i)
  }


  for(i=0;i<buttons.length;i++) {
    buttons[i].addEventListener('click', function(e) {
      mycopy(e.target)
    })
  }
}
