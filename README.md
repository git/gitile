Gitile
======

Gitile (pronounced /*gitil*/) is a small git forge written in
[Guile Scheme](https://gnu.org/software/guile).  It can currently show public
repository contents on a website.  Visit [this project's repository](https://git.lepiller.eu/gitile)
to have a look at what it can do.

How to Build Gitile
-------------------

Gitile requires Guile of course, in addition to the following Guile libraries:

* guile-git (for accessing git repositories)
* guile-commonmark (for markdown highlighting)
* guile-syntax-highlight (for other syntax highlighting.  Use commit
  `51727cbb7fc05ef743aab2d7b16314ea1ed790e4`, it is more recent than the
  latest release)
* guile-fibers (for running a multithreaded web server)
* guile-gcrypt (for a hash computation)

You will also need the usual suspects:

* autotools
* make
* pkg-config

From this repository, run the following:

```bash
./bootstrap
./configure
make
```

To install run:

```bash
make install
```

How to Run Gitile?
------------------

Before running gitile, you will need a configuration file.  The configuration
is a record that contains all of the following keys, as in the following
example:

```scheme
;; gitile.conf
(config
  (port 8080)
  (host "localhost")
  (database "")
  (repositories "/srv/git")
  (base-git-url "https://git.example.org/git")
  (index-title "My projects")
  (intro ((p "Content of the intro on index page")))
  (footer ((p "Footer content")))))
```

* port: the port on which to listen
* host: the host on which to listen
* database: currently unused
* repositories: The directory in which public repositories can be found
* base-git-url: The base url for cloning repositories
* index-title: The title for the index page
* intro: The content of the introduction text on the index page.  It is a list
  of sxml expressions.
* footer: The content of the footer text on all pages. It is also a list of
  sxml expressions.

Save the file as `gitile.conf` (or whatever name you like) and run gitile with

```bash
./pre-inst-env scripts/gitile -c gitile.conf
```

It should be running on `localhost:8080`, but you will not get any assets.

To get assets, you need to have them served, for instance with Nginx acting as
a reverse proxy for gitile, and serving the assets directly.

Repository Configuration
------------------------

In order to show information such as a description, a nice name, and a short
summary for each repository, you need to configure it in the repository, with
`git config`.  We use the `gitweb.*` configuration space for that.

* gitweb.description: The long description shown on the index page, it can
  contain arbitrary html tags.
* gitweb.synopsis: The short summary shown on the project page.
* gitweb.name: The nice name shown on the index page.

For instance, you can set the following on a repository:

```bash
cd /srv/git/my-repo
git config --local gitweb.description "My repository is a wonderful project that will <b>conquer</b> the world!"
git config --local gitweb.synopsis "A repo to conquer the world"
git config --local gitweb.name "My Repository"
```
