;;;; Copyright (C) 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;;;

(use-modules (guix packages)
             (guix licenses)
             (guix build-system gnu)
             (guix git-download)
             (guix git)
             (gnu packages autotools)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages tls))

(define my-guile-syntax-highlight
  (package
    (inherit guile-syntax-highlight)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://git.dthompson.us/guile-syntax-highlight.git")
                     (commit "897fa5156ff41588e0d281eb00e4e94de63ccd8a")))
              (file-name (git-file-name "guile-syntax-highlight" "0.1.897fa51"))
              (sha256
               (base32
                "18zlg4mkgd3swgv2ggfz91ivnnzc0zhvc9ybgrxg1y762va9hyvj"))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("texinfo" ,texinfo)
       ,@(package-native-inputs guile-syntax-highlight)))))

(package
  (name "gitile")
  (version "0.1")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (arguments
   `(#:modules ((guix build utils)
                (guix build gnu-build-system)
                (ice-9 rdelim)
                (ice-9 popen))
     #:make-flags (list "GUILE_AUTO_COMPILE=0")
     #:phases
     (modify-phases %standard-phases
       (add-after 'install-bin 'wrap-program
         (lambda* (#:key inputs outputs #:allow-other-keys)
           ;; Wrap the 'gitile' command to refer to the right modules.
           (let* ((out    (assoc-ref outputs "out"))
                  (commonmark (assoc-ref inputs "guile-commonmark"))
                  (git    (assoc-ref inputs "guile-git"))
                  (bytes  (assoc-ref inputs "guile-bytestructures"))
                  (fibers (assoc-ref inputs "guile-fibers"))
                  (gcrypt (assoc-ref inputs "guile-gcrypt"))
                  (syntax-highlight (assoc-ref inputs "guile-syntax-highlight"))
                  (deps   (list out commonmark git bytes fibers gcrypt
                                syntax-highlight))
                  (guile  (assoc-ref %build-inputs "guile"))
                  (effective (read-line
                              (open-pipe* OPEN_READ
                                          (string-append guile "/bin/guile")
                                          "-c" "(display (effective-version))")))
                  (mods   (string-drop-right  ;drop trailing colon
                           (string-join deps
                                        (string-append "/share/guile/site/"
                                                       effective ":")
                                        'suffix)
                           1))
                  (objs   (string-drop-right
                           (string-join deps
                                        (string-append "/lib/guile/" effective
                                                       "/site-ccache:")
                                        'suffix)
                           1)))
             (wrap-program (string-append out "/bin/gitile")
               `("GUILE_LOAD_PATH" ":" prefix (,mods))
               `("GUILE_LOAD_COMPILED_PATH" ":" prefix (,objs)))))))))
  (propagated-inputs
   `(("guile-commonmark" ,guile-commonmark)
     ("guile-git" ,guile-git)
     ("guile-gcrypt" ,guile-gcrypt)
     ("guile-syntax-highlight" ,my-guile-syntax-highlight)
     ("gnutls" ,gnutls)
     ("guile-fibers" ,guile-fibers)))
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("libtool" ,libtool)
     ("pkg-config" ,pkg-config)
     ("guile" ,guile-3.0)))
  (home-page "https://git.lepiller.eu")
  (synopsis "")
  (description "")
  (license gpl3+))
